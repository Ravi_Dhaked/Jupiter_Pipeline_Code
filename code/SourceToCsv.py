from pyspark.sql import SparkSession
from pyspark.sql.types import *
from pyspark.sql.functions import *
import pandas as pd
import glob

spark = SparkSession \
    .builder \
    .appName('CN_Avanse_Csv') \
    .config("spark.some.config.option", "some-value") \
    .getOrCreate()

Pathfile = '/home/hadoop/Jupiter_Money_Process/SourceData/Source/'
CsvFile = '/home/hadoop/Jupiter_Money_Process/SourceData/CSV/'

# -------------------------------------------------------- Loan_Master-----------------------------------------------------------------------


if len(glob.glob(Pathfile+'Loan_Master*.csv')) >= 1:
    Loan_Master = pd.concat([pd.read_csv(files)
                    for files in glob.glob(Pathfile+'Loan_Master*.csv')])
    Loan_Master.to_csv(CsvFile+'Loan_Master.csv', index=None)

# --------------------------------------------------------Dpd_Data_Details-----------------------------------------------------------------------


if len(glob.glob(Pathfile+'Dpd_Data_Details*.csv')) >= 1:
    Dpd_Data_Details = pd.concat([pd.read_csv(files)
                             for files in glob.glob(Pathfile+'Dpd_Data_Details*.csv')])
    Dpd_Data_Details.to_csv(CsvFile+'Dpd_Data_Details.csv', index=None)

# --------------------------------------------------------Loan_Master_Extension-----------------------------------------------------------------------

if len(glob.glob(Pathfile+'Loan_Master_Extension*.csv')) >= 1:
    Loan_Master_Extension = pd.concat([pd.read_csv(files)
                               for files in glob.glob(Pathfile+'Loan_Master_Extension*.csv')])
    Loan_Master_Extension.to_csv(CsvFile+'Loan_Master_Extension.csv', index=None)


spark.stop()

from functools import reduce
from pyspark.sql.types import StructType, StructField, StringType
from pyspark.sql import DataFrame
from pyspark.sql import SparkSession
from pyspark.sql.functions import *
from pyspark.sql.types import *
import datetime
import pyspark.sql.functions as F
import pandas as pd
import random
# importing files
import jupiter_Variables
import jupiter_generalFunctions
import jupiter_getActiveAccounts
from pyspark.sql.types import DoubleType, IntegerType, DateType



 
def mergeBothFile(totalActive: DataFrame, mainFile: DataFrame, monthlyFile: DataFrame,spark: SparkSession) -> DataFrame:
        
    mainFile = mainFile.select("customerId","disbursementAmount","disbursementDate","firstDisbDate","lastPaidDay",
                               "loanAmount","loanId","loanType","name","phoneNumber","pincode","principal",
                               "product","billDate","dueDate")
                               
               
    monthlyFile= monthlyFile.select("loanId","repaymentAmountSinceLastBillDate","maxBillDueAsOnDate","maxBillDueAsOnBillGenerated","billBucket1","billBucket2","billBucket3","asbillBucket4","billBucket5","billBucket6","billBucket7",
    "billBucket8","npaFlag","writeoffFlag","billCycleGroup","customerRiskIndicator1","customerRiskIndicator2",
    "LTD_Payments","LTD_RePayments","LTD_charges","LTD_waviers_Income")\
            .dropDuplicates(["loanId"])        


    totalActive= totalActive.select("bucket","dailyDate","dpd","loanId","outstandingAmount","overDueAmount","DueAmount")\
                            .dropDuplicates(["loanId"])


    mergeWithMainFile = mainFile.join(monthlyFile,"loanId") .dropDuplicates(["loanId"])
 
    mainFilePart1 = mergeWithMainFile.join(totalActive,"loanId") .dropDuplicates(["loanId"])
    mainFileNew = mainFilePart1


                                               
    mainFileNew = mainFilePart1.select("customerId","disbursementAmount","disbursementDate","firstDisbDate","lastPaidDay",
                               "loanAmount","loanId","loanType","name","phoneNumber","pincode","principal",
                               "product","billDate","dueDate",
                               "billBucket1","billBucket2","billBucket3","asbillBucket4","billBucket5",
                               "billBucket6","billBucket7",
                               "billBucket8","npaFlag","writeoffFlag","billCycleGroup","customerRiskIndicator1","customerRiskIndicator2",
                               "repaymentAmountSinceLastBillDate","maxBillDueAsOnDate","maxBillDueAsOnBillGenerated",
                               "LTD_Payments","LTD_RePayments","LTD_charges","LTD_waviers_Income","bucket","dailyDate","DueAmount",
                               "dpd","outstandingAmount","overDueAmount").withColumn("actions",lit("TB"))\
                             .withColumn("message",lit("None")).withColumn("message_type",lit("None"))\
                             .withColumn("bucket_movement",lit("NL"))\
                            .withColumn("Status",lit("OUTSTANDING"))\
                            .withColumnRenamed("customerId","customer_id").withColumnRenamed("disbursementAmount","disbursal_amount")\
                            .withColumnRenamed("disbursementDate","disbursal_date").withColumnRenamed("loanAmount","loan_amount")\
                            .withColumnRenamed("outstandingAmount","outstanding_amount").withColumnRenamed("overDueAmount","over_due_amount")\
                            .withColumnRenamed("name","customer_name").withColumnRenamed("phoneNumber","mobile")\
                            .withColumnRenamed("dueDate","due_date").withColumnRenamed("loanId","loan_account_number").withColumnRenamed("DueAmount","due_amount")\



    return mainFileNew



def getAllAdditinalColumns(totalActiveAccounts: DataFrame, spark: SparkSession) -> DataFrame:
    
    mainFile = spark.read.csv(jupiter_Variables.Loan_Master_Filepath,header=True, inferSchema=True)
    monthlyFile = spark.read.csv(jupiter_Variables.Loan_Master_Extension_Filepath, header=True, inferSchema=True)
    totalActive = spark.read.csv(jupiter_Variables.DPD_Data_Filepath, header=True, inferSchema=True)
    df = mergeBothFile( totalActive,mainFile, monthlyFile, spark)
    # df = df.withColumn("disbursal_date", to_date(col("disbursal_date"), 'dd-MM-yy'))
    # df = df.withColumn("disbursal_date",date_format('disbursal_date', 'yy-MM-dd'))  
    df = df.withColumn("action_date", current_date())
 
    df = df.withColumn("Allocation_Bucket",col("maxBillDueAsOnDate"))
    df = df.withColumn("Allocation_Amount",col("outstanding_amount").cast("int"))

    print(df.dtypes)

    df = df.withColumn("Status",when(((col("over_due_amount") < 100) & (col("due_amount") > 100)),lit('DPD_Resolved'))
                   .when(((col("over_due_amount") < 100) & (col("due_amount") < 100)),lit('PaidbeforeDueDate'))
                   .when(((col("over_due_amount") > 100) & (col("repaymentAmountSinceLastBillDate") > 0)),lit('PartialPayment'))
                   .when(((col("over_due_amount") > 100) & (col("repaymentAmountSinceLastBillDate")== 0)),lit('Unresolved')))

    # df = df.select("customer_id","disbursal_amount","disbursal_date","firstDisbDate","lastPaidDay",
             # "loan_amount","loan_account_number","loanType","customer_name","mobile","pincode","principal",
             # "product","billDate","due_date",
             # "billBucket1","billBucket2","billBucket3","asbillBucket4","billBucket5",
             # "billBucket6","billBucket7",
             # "billBucket8","npaFlag","writeoffFlag","billCycleGroup","customerRiskIndicator1","customerRiskIndicator2",
             # "repaymentAmountSinceLastBillDate","maxBillDueAsOnDate","maxBillDueAsOnBillGenerated",
             # "LTD_Payments","LTD_RePayments","LTD_charges","LTD_waviers_Income","bucket","dailyDate",
             # "dpd","outstanding_amount","over_due_amount","actions","message","message_type")
    
    df = df.select("actions","action_date","bucket","customer_name","disbursal_amount","disbursal_date","dpd","due_date","loan_amount","loan_account_number",
                   "mobile","outstanding_amount","over_due_amount","pincode","product","customer_id","Status","bucket_movement","LTD_Payments","LTD_RePayments","LTD_charges","LTD_waviers_Income",
                   "repaymentAmountSinceLastBillDate","maxBillDueAsOnDate","maxBillDueAsOnBillGenerated","billBucket1","billBucket2","billBucket3",
                   "asbillBucket4","billBucket5","billBucket6","billBucket7","billBucket8","due_amount","Allocation_Bucket","Allocation_Amount")
    return df



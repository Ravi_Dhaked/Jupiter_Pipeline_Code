from functools import reduce
from pyspark.sql import DataFrame
import datetime

# merge two dataframe


def unionAll(*dfs):
    return reduce(DataFrame.unionAll, dfs)

# general function for joining two dataFrame


def joinTwoDataframe(leftDataFrame: DataFrame, rightDataFrame: DataFrame, commonColumn, joinType) -> DataFrame:
    return leftDataFrame.join(rightDataFrame, [commonColumn], joinType)

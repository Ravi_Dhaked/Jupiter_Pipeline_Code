from functools import reduce
from pyspark.sql.types import StructType, StructField, StringType
from pyspark.sql import DataFrame
from pyspark.sql import SparkSession
from pyspark.sql.functions import *
from pyspark.sql.types import *
import datetime
import jupiter_Variables
import jupiter_generalFunctions


def getTotalActiveAccounts(spark: SparkSession) -> DataFrame:
    df = spark.read.csv(jupiter_Variables.Loan_Master_Filepath,header=True,
                        inferSchema=True).select("loanId").dropDuplicates(["loanId"])
    print("Total_Active_Accounts_After_Union_All_Three_Segment ", df.count())
    return df

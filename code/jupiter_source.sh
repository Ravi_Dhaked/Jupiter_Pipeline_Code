sudo rm -rvf /home/hadoop/Jupiter_Money_Process/SourceData/Source/*.csv
sudo rm -rvf /home/hadoop/Jupiter_Money_Process/SourceData/CSV/*.csv

fn=`date +"%Y%m%d"`
fn1=`date +"%Y-%m-%d"`
sudo aws s3 cp s3://cn-jupiter-money/row_data/$fn/ /home/hadoop/cn-jupiter-money/row_data/$fn1/ --recursive

sudo spark-submit --master yarn --deploy-mode client --queue default --num-executors 5 --executor-memory 1G --executor-cores 2 --conf spark.sql.shuffle.partitions=10 --conf spark.default.parallelism=10 /home/hadoop/Jupiter_Money_Process/SourceData/SourceToCsv.py

ret_code=$?
  if [ $ret_code -ne 0 ]; then 
     exit $ret_code
  fi
  


sudo aws s3 cp /home/hadoop/cn-jupiter-money/row_data/$fn1/ s3://Jupiter_Money_Automation/SourceData/CSV/ --recursive
sudo aws s3 cp /home/hadoop/cn-jupiter-money/row_data/$fn1/ s3://Jupiter_Money_Automation/input/$(date '+%d-%m-%Y')/ --recursive

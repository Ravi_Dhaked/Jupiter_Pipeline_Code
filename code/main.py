from functools import reduce
from pyspark.sql.types import StructType, StructField, StringType
from pyspark.sql import DataFrame
from pyspark.sql import SparkSession
from pyspark.sql.functions import *
from pyspark.sql.types import *
import datetime


# importing files
import jupiter_Variables
import jupiter_generalFunctions
import jupiter_getActiveAccounts
#import getAdditionalColumns
#import actions
import jupiter_additional

# starting session

spark = SparkSession \
    .builder \
    .appName('Mahindra-main') \
     .config('spark.sql.debug.maxToStringFields', 2000)\
     .config('spark.debug.maxToStringFields', 2000)\
     .config('spark.sql.codegen.wholeStage', 'false')\
     .config('spark.sql.autoBroadcastJoinThreshold', -1)\
     .config('spark.sql.thriftServer.incrementalCollect','true')\
     .getOrCreate()
spark.sql("set spark.sql.legacy.timeParserPolicy=LEGACY")
   
             
totalActiveAccounts = getActiveAccounts.getTotalActiveAccounts(spark)


finalDF = jupiter_additional.getAllAdditinalColumns(totalActiveAccounts, spark)
finalDF.coalesce(1).write.option("delimiter", "|").csv('s3://temp-project-m/practice/Output/jupiter_additional_output', header=True)
print("Done")

spark.stop()
exit()

